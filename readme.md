### 0 : créer un groupe du nombre de personnes voulu entre 3 et 6 

### 1 : chaque groupe doit avoir un repo git ou les différents fichiers du rendu seront stockes

### 2 : m'envoyer un email avec les membres et l'adresse du repo

### 3 : Créer un script d'installation de minikube 'install-minikube.sh'
- À partir d'une vm ubuntu vierge le script devra installer les différents éléments nécessaires a l'exécution de minikube

### 4 : Créer un script avec les fichiers necessaires 'minikube-pods.sh'
- création d'un namespace
- création des services
  - vuejs
  - strapi
  - postgres
- déploiement des services

### 5 : Creer un script pour lancer le dashboard kubernetes

- Pour toutes questions contactez moi par mail a cette adresse pierredamiendelbreil@gmail.com
- Lors de la soutenance, je clonerai votre repo et exécuterai les différents scripts
- Ensuite je vous poserai quelques questions sur le fonctionnement des différents éléments du projet
- Je vous enverrai les ordres de passage courant de la semaine prochaine quand vous aurez défini les groupes

Bon courage.

